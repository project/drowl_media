const { watch, series, parallel, src, dest, lastRun } = require("gulp");

// =================================
// Configuration
// =================================

// List the module ('./') and submodule directories here, its important to have the
// "js", "css" & "scss" folders as direct children of this folders.
const folders = [".", "modules/drowl_media_video", "modules/drowl_media_types"];

// Console colors (see: https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color)
const successColor = "\x1b[32m";
const infoColor = "\x1b[36m";
const warningColor = "\x1b[33m";
const alertColor = "\x1b[33m";

// Plugins
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const babel = require("gulp-babel");
const gulpRemoveLogging = require("gulp-remove-logging");
const sass = require("gulp-sass")(require("sass"));
const postcss = require("gulp-postcss");
const cleanCss = require("gulp-clean-css");
const autoprefixer = require("autoprefixer");
const path = require("path");
const notify = require("gulp-notify");

// Scripts Tasks
function buildScripts(done) {
  if (folders.length === 0) return done(); // nothing to do!
  var tasks = folders.map(function (folder) {
    return (
      src(
        [
          path.join(folder, "/js/**/*.js"),
          "!" + path.join(folder, "/js/**/*.min.js"),
        ],
        {
          since: lastRun(buildScripts),
        }
      )
        .pipe(plumber())
        // Removed minify - let drupal minify the JS files.
        // .pipe(
        //   rename({
        //     suffix: ".min",
        //   })
        // )
        // Remove console logs
        .pipe(gulpRemoveLogging())
        // Transcompile ES6 code
        // !! DONT use this Babel preset in DRUPAL, it adds a "use stict", what triggers a
        // !! CORE ISSUE. ES6 support is fine. Skip this.
        // .pipe(
        //   babel({
        //     presets: ["@babel/env"],
        //   })
        // )
        // Minitfy / Uglify
        // Removed minify - let drupal minify the JS files.
        // .pipe(uglify())
        .pipe(
          notify({
            message: "Build Scripts: " + folder + "/ finished!",
            onLast: true,
          })
        )
        .pipe(dest(folder + "/js"))
    );
  });
  return [tasks, done()];
}

function buildSass(done) {
  if (folders.length === 0) return done(); // nothing to do!
  var tasks = folders.map(function (folder) {
    return (
      src([path.join(folder, "/scss/**/*.scss")], {
        since: lastRun(buildSass),
      })
        .pipe(plumber())
        .pipe(sass.sync().on("error", sass.logError))
        .pipe(
          postcss([autoprefixer(), require("postcss-nested")], {
            syntax: require("postcss-scss"),
          })
        )
        // Minitfy / Uglify
        .pipe(
          rename({
            suffix: ".min",
          })
        )
        .pipe(cleanCss())
        .pipe(
          notify({
            message: "SASS: " + folder + "/ finished!",
            onLast: true,
          })
        )
        .pipe(dest(folder + "/css"))
    );
  });
  return [tasks, done()];
}

exports.build = function (done) {
  var doBuild = series(buildScripts, buildSass);
  doBuild();
  done();
};
