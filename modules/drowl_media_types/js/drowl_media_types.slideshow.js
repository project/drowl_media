/**
 * @file
 * Various Media Library Scripts
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_media_types_slideshow = {
    attach: function (context, settings) {
      // Add animation class to the image or video video field wrappers in media-slides
      let $mediaSlidesWithAnimation = $(
        ".media-slide__media[data-media-animation]",
        context
      );
      if ($mediaSlidesWithAnimation.length) {
        $mediaSlidesWithAnimation.each(function () {
          let $mediaSlideMediaWrapper = $(this);
          // Add the animation class to the media field wrapper (image or video)
          if ($mediaSlideMediaWrapper.parents(".slick__slider:first").length) {
            // Media "Slides" also works outside slideshows.
            // If the media-slide is located inside a slick-carousel/slideshow
            // we will add/remove the animation classes dynamically
            // to increase the performance of sites with a huge amount
            // of animated media-slides.
            $mediaSlideMediaWrapper
              .parents(".slick__slider:first")
              .addClass("has-animated-media-slides");
          } else {
            // Single Slides outside a Slick slideshow > fire animtaion directly.
            $mediaSlideMediaWrapper
              .find(".animation-wrapper:first > :first-child", context)
              .addClass("animation-" + $(this).attr("data-media-animation"));
          }
        });
        $(".slick__slider.has-animated-media-slides", context).on(
          "beforeChange",
          function (event, slick, currentSlide, nextSlide) {
            let $nextSlide = $(slick.$slides.get(nextSlide));
            // Remove class from current slide
            // add transition time as delay to prevent fouc
            setTimeout(function () {
              $(slick.$slides.get(currentSlide))
                .find(".animation-wrapper:first > :first-child", context)
                .removeClass("animation-ken-burns");
            }, slick.slickGetOption("speed"));
            // Add Animation class to next slide
            $nextSlide
              .find(".animation-wrapper:first > :first-child", context)
              .addClass(
                "animation-" +
                  $nextSlide
                    .find(".media-slide__media--animated:first")
                    .attr("data-media-animation")
              );
          }
        );
      }
    },
  };
})(jQuery, Drupal);
