<?php

namespace Drupal\drowl_media_types;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides the static and selected default values for the media type fields.
 */
class DrowlMediaTypesFieldValuesProvider {

  public static function getImageAnimationValues(): array {
    return [
      'ken-burns' => new TranslatableMarkup('Ken-Burns Effect')
    ];
  }

  public static function getOverlayButtonColorValues(): array {
    return [
      'light' => new TranslatableMarkup('Light'),
      'dark' => new TranslatableMarkup('Dark'),
    ];
  }

  public static function getOverlayButtonStyleValues(): array {
    return [
      'regular' => new TranslatableMarkup('Regular'),
      'hollow' => new TranslatableMarkup('Hollow (only border, no background)'),
    ];
  }

  public static function getOverlayDisplayValues(): array {
    return [
      'light' => new TranslatableMarkup('Box light, Text dark'),
      'dark' => new TranslatableMarkup('Box dark, Text light'),
      'light-glass' => new TranslatableMarkup('Box transparent-light, Text dark'),
      'dark-glass' => new TranslatableMarkup('Box transparent-dark, Text light'),
      'transparent-light' => new TranslatableMarkup('Box transparent (100%), Text dark'),
      'transparent-dark' => new TranslatableMarkup('Box transparent (100%), Text light'),
      'primary' => new TranslatableMarkup('Highlight / Primary color'),
      'secondary' => new TranslatableMarkup('Secondary color'),
    ];
  }

  public static function getOverlayPositionValues(): array {
    return [
      'disabled' => new TranslatableMarkup('No overlay'),
      'top-left' => new TranslatableMarkup('Top left'),
      'top-center' => new TranslatableMarkup('Top center'),
      'top-right' => new TranslatableMarkup('Top right'),
      'middle-left' => new TranslatableMarkup('Middle left'),
      'middle-center' => new TranslatableMarkup('Middle center'),
      'middle-right' => new TranslatableMarkup('Middle right'),
      'bottom-left' => new TranslatableMarkup('Bottom left'),
      'bottom-center' => new TranslatableMarkup('Bottom center'),
      'bottom-right' => new TranslatableMarkup('Bottom right'),
    ];
  }

  public static function getOverlaySizingValues(): array {
    return [
      'seperate-box' => new TranslatableMarkup('Separate Box'),
      'full-size' => new TranslatableMarkup('Full-Size'),
    ];
  }

  public static function getSlideYesNoValues(): array {
    return [
      'default' => new TranslatableMarkup('Slick Optionset default'),
      '1' => new TranslatableMarkup('Yes'),
      '0' => new TranslatableMarkup('No'),
    ];
  }

  public static function getSlideHeightValues(): array {
    return [
      'limited' => new TranslatableMarkup('Limited height'),
      'auto' => new TranslatableMarkup('Automatic (highest image height)'),
      'viewport' => new TranslatableMarkup('100% of the viewport height (browser height)'),
    ];
  }

  public static function getImageAnimationDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.image_animation');
  }

  public static function getOverlayButtonColorDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_button_color');
  }

  public static function getOverlayButtonStyleDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_button_style');
  }

  public static function getOverlayDisplayDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_display');
  }

  public static function getOverlayPositionLgDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_position_lg');
  }

  public static function getOverlayPositionMdDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_position_md');
  }

  public static function getOverlayPositionDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_position');
  }

  public static function getOverlaySizingLgDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing_lg');
  }

  public static function getOverlaySizingMdDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing_md');
  }

  public static function getOverlaySizingDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing');
  }

  public static function getSlideArrowsDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slideshow.slide_arrows');
  }

  public static function getSlideAutoplayDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slideshow.slide_autoplay');
  }

  public static function getSlideDotsDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slideshow.slide_dots');
  }

  public static function getSlideHeightDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slideshow.slide_height');
  }

  public static function getSlideInfiniteDefaultValue(): string {
    return \Drupal::config('drowl_media_types.settings')->get('defaults.slideshow.slide_infinite');
  }

}
