<?php

declare(strict_types=1);

namespace Drupal\drowl_media_types\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drowl_media_types\DrowlMediaTypesFieldValuesProvider;

/**
 * Settings page for the DROWL Media Types module.
 */
final class DrowlMediaTypesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'drowl_media_types_drowl_media_types_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['drowl_media_types.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['defaults'] = [
      '#type' => 'details',
      '#title' => $this->t('DROWL Media Types defaults'),
      '#open' => TRUE,
    ];

    $form['defaults']['slide'] = [
      '#type' => 'details',
      '#title' => $this->t('Slide'),
      '#open' => TRUE,
    ];
    $form['defaults']['slide']['image_animation'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Animation'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getImageAnimationValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.image_animation'),
      '#required' => TRUE,
    ];
    $form['defaults']['slide']['overlay'] = [
      '#type' => 'details',
      '#title' => $this->t('Overlay'),
      '#open' => TRUE,
    ];
    $form['defaults']['slide']['overlay']['overlay_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Display'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayDisplayValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_display'),
      '#required' => TRUE,
      '#description' => $this->t('Specifies whether the “Light” or “Dark” display should be used. “Light” creates a light box with dark text, ‘Dark’ creates a dark box with light text.
      Use this setting to optimize the legibility of text depending on the subject.'),
    ];
    $form['defaults']['slide']['overlay']['overlay_button_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Button Color'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayButtonColorValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_button_color'),
      '#required' => TRUE,
      '#description' => $this->t('Select the color that matches the background color for the button.'),
    ];
    $form['defaults']['slide']['overlay']['overlay_button_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Button Style'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayButtonStyleValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_button_style'),
      '#required' => TRUE,
    ];    
    $form['defaults']['slide']['overlay']['devices'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-small',
      '#description' => $this->t('Overlay settings for small devices and upwards (if no different setting is made for larger devices, the settings for larger devices are applied).'),
    ];
    $form['defaults']['slide']['overlay']['devices']['small'] = [
      '#type' => 'details',
      '#title' => $this->t('Small devices'),
      '#open' => TRUE,
      '#group' => 'devices',
    ];
    $form['defaults']['slide']['overlay']['devices']['small']['overlay_position_small'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Position'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayPositionValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_position'),
      '#required' => TRUE,
      '#description' => $this->t('Select whether an overlay (superimposed text area) is used and where it is placed on the image.'),
    ];
    $form['defaults']['slide']['overlay']['devices']['small']['overlay_sizing_small'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Sizing'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlaySizingValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing'),
      '#required' => TRUE,
      '#description' => $this->t('The overlay can be displayed as a separate box or flat. When displayed as a separate box in the selected display type around the text, the image / video in the background is not affected. In the two-dimensional display, on the other hand, the box covers the entire surface of the image / video; if an “overlay display” is selected, which has a background color, e.g. “black transparent”, the image / video is overlaid by this. In this example, the image / video would be darkened to make the text easier to read.'),
    ];
    $form['defaults']['slide']['overlay']['devices']['medium'] = [
      '#type' => 'details',
      '#title' => $this->t('Medium devices'),
      '#open' => TRUE,
      '#group' => 'devices',
    ];
    $form['defaults']['slide']['overlay']['devices']['medium']['overlay_position_medium'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Position'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayPositionValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_position_md'),
      '#required' => TRUE,
      '#description' => $this->t('Select whether an overlay (superimposed text area) is used and where it is placed on the image.'),
    ];
    $form['defaults']['slide']['overlay']['devices']['medium']['overlay_sizing_medium'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Sizing'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlaySizingValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing_md'),
      '#required' => TRUE,
      '#description' => $this->t('The overlay can be displayed as a separate box or flat. When displayed as a separate box in the selected display type around the text, the image / video in the background is not affected. In the two-dimensional display, on the other hand, the box covers the entire surface of the image / video; if an “overlay display” is selected, which has a background color, e.g. “black transparent”, the image / video is overlaid by this. In this example, the image / video would be darkened to make the text easier to read.'),
    ];
    $form['defaults']['slide']['overlay']['devices']['large'] = [
      '#type' => 'details',
      '#title' => $this->t('Large devices'),
      '#open' => TRUE,
      '#group' => 'devices',
    ];
    $form['defaults']['slide']['overlay']['devices']['large']['overlay_position_large'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Position'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlayPositionValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_position_lg'),
      '#required' => TRUE,
      '#description' => $this->t('Select whether an overlay (superimposed text area) is used and where it is placed on the image.'),
    ];
    $form['defaults']['slide']['overlay']['devices']['large']['overlay_sizing_large'] = [
      '#type' => 'select',
      '#title' => $this->t('Overlay Sizing'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getOverlaySizingValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slide.overlay_sizing_lg'),
      '#required' => TRUE,
      '#description' => $this->t('The overlay can be displayed as a separate box or flat. When displayed as a separate box in the selected display type around the text, the image / video in the background is not affected. In the two-dimensional display, on the other hand, the box covers the entire surface of the image / video; if an “overlay display” is selected, which has a background color, e.g. “black transparent”, the image / video is overlaid by this. In this example, the image / video would be darkened to make the text easier to read.'),
    ];

    $form['defaults']['slideshow'] = [
      '#type' => 'details',
      '#title' => $this->t('Slideshow'),
      '#open' => TRUE,
    ];
    $form['defaults']['slideshow']['slide_height'] = [
      '#type' => 'select',
      '#title' => $this->t('Slideshow Height'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getSlideHeightValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slideshow.slide_height'),
      '#required' => TRUE,
      '#description' => $this->t('Select the slideshow\'s height. The “Limited height” option uses a fixed value that is stored in the Drupal template.'),
    ];
    $form['defaults']['slideshow']['slide_autoplay'] = [
      '#type' => 'select',
      '#title' => $this->t('Autoplay'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getSlideYesNoValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slideshow.slide_autoplay'),
      '#required' => TRUE,
      '#description' => $this->t('Plays the slideshow automatically. If this option is deactivated, the user must switch manually.')
        . ' ' . $this->t('"Slick Optionset default" uses the Slick defaults configured in the Slick Optionset. <a href="/admin/config/media/slick/paragraphs_slide_container" target="_blank">These defaults can be changed here</a>'),
    ];
    $form['defaults']['slideshow']['slide_arrows'] = [
      '#type' => 'select',
      '#title' => $this->t('Arrows'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getSlideYesNoValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slideshow.slide_arrows'),
      '#required' => TRUE,
      '#description' => $this->t('Determines whether the forward and back navigation arrows should be displayed')
        . ' ' . $this->t('"Slick Optionset default" uses the Slick defaults configured in the Slick Optionset. <a href="/admin/config/media/slick/paragraphs_slide_container" target="_blank">These defaults can be changed here</a>'),
    ];    
    $form['defaults']['slideshow']['slide_dots'] = [
      '#type' => 'select',
      '#title' => $this->t('Dots'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getSlideYesNoValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slideshow.slide_dots'),
      '#required' => TRUE,
      '#description' => $this->t('Determines whether the point navigation is displayed below the slideshow.')
        . ' ' . $this->t('"Slick Optionset default" uses the Slick defaults configured in the Slick Optionset. <a href="/admin/config/media/slick/paragraphs_slide_container" target="_blank">These defaults can be changed here</a>'),
    ];    
    $form['defaults']['slideshow']['slide_infinite'] = [
      '#type' => 'select',
      '#title' => $this->t('Infinite'),
      '#options' => DrowlMediaTypesFieldValuesProvider::getSlideYesNoValues(),
      '#default_value' => $this->config('drowl_media_types.settings')->get('defaults.slideshow.slide_infinite'),
      '#required' => TRUE,
      '#description' => $this->t('Display as an infinite loop. After the last element, start again at the first.')
        . ' ' . $this->t('"Slick Optionset default" uses the Slick defaults configured in the Slick Optionset. <a href="/admin/config/media/slick/paragraphs_slide_container" target="_blank">These defaults can be changed here</a>'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('drowl_media_types.settings')
      ->set('defaults.slide.image_animation', $form_state->getValue('image_animation'))
      ->set('defaults.slide.overlay_button_color', $form_state->getValue('overlay_button_color'))
      ->set('defaults.slide.overlay_button_style', $form_state->getValue('overlay_button_style'))
      ->set('defaults.slide.overlay_display', $form_state->getValue('overlay_display'))
      ->set('defaults.slide.overlay_position_lg', $form_state->getValue('overlay_position_large'))
      ->set('defaults.slide.overlay_position_md', $form_state->getValue('overlay_position_medium'))
      ->set('defaults.slide.overlay_position', $form_state->getValue('overlay_position_small'))
      ->set('defaults.slide.overlay_sizing_lg', $form_state->getValue('overlay_sizing_large'))
      ->set('defaults.slide.overlay_sizing_md', $form_state->getValue('overlay_sizing_medium'))
      ->set('defaults.slide.overlay_sizing', $form_state->getValue('overlay_sizing_small'))
      ->set('defaults.slideshow.slide_arrows', $form_state->getValue('slide_arrows'))
      ->set('defaults.slideshow.slide_autoplay', $form_state->getValue('slide_autoplay'))
      ->set('defaults.slideshow.slide_dots', $form_state->getValue('slide_dots'))
      ->set('defaults.slideshow.slide_height', $form_state->getValue('slide_height'))
      ->set('defaults.slideshow.slide_infinite', $form_state->getValue('slide_infinite'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
