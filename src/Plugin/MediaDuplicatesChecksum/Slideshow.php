<?php

namespace Drupal\drowl_media\Plugin\MediaDuplicatesChecksum;

use Drupal\media\Entity\Media;
use Drupal\media_duplicates\Plugin\MediaDuplicatesChecksumBase;

/**
 * Slideshow duplicates checksum.
 *
 * @MediaDuplicatesChecksum(
 *   id = "slideshow",
 *   label = @Translation("Slideshow"),
 *   media_types = {"slideshow"},
 * )
 */
class Slideshow extends MediaDuplicatesChecksumBase {

  /**
   * {@inheritdoc}
   */
  public function getChecksumData(Media $media) {
    $source = $media->getSource();
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList*/
    $sourceFieldValue = $source->getSourceFieldValue($media);
    // @todo Add setting conditions from
    // https://www.drupal.org/project/drowl_media/issues/3305117 here!
    $referencedEntities = $sourceFieldValue->referencedEntities();
    $referencedEntitiesUuidsConcat = '';
    // The unique identfifier for the list of referenced entities is
    // their concat uuid in the lists order.
    foreach ($referencedEntities as $referencedEntity) {
      $referencedEntitiesUuidsConcat .= empty($referencedEntitiesUuidsConcat) ? '' : '|';
      $referencedEntitiesUuidsConcat .= $referencedEntity->uuid();
    }
    return $referencedEntitiesUuidsConcat;

  }

}
