<?php

namespace Drupal\drowl_media\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ItemsCount constraint.
 *
 * This is from https://www.drupal.org/project/media_entity_slideshow in
 * large parts.
 */
class ItemsCountConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (!isset($value)) {
      return;
    }

    if ($value->get($constraint->sourceFieldName)->isEmpty()) {
      $this->context->addViolation($constraint->message);
    }
  }

}
