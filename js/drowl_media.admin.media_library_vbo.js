/**
 * @file
 * This file is only attached, if the "views_bulk_operations/frontUi" library is present on the page.
 * Adds click-selection for media library item checkboxes when using VBO.
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_media_library_vbo = {
    attach: function (context, settings) {
      // Make VBO act like the core bulk operations (click on the .media-library-item toggles the checkbox)
      $(".vbo-view-form .media-library-item", context).each(function () {
        let $mediaItem = $(this);
        let $vboSelectionTrigger = $mediaItem.find(
          ".media-library-item__preview-wrapper:first"
        );
        let $vboCheckbox = $mediaItem.find("input.form-checkbox:first");
        // Clicking the .media-library-item
        $vboSelectionTrigger.on("click", function (e) {
          e.preventDefault();
          e.stopPropagation();
          if ($vboCheckbox.is(":checked")) {
            $vboCheckbox.prop("checked", false);
            $mediaItem.removeClass("checked");
          } else {
            $vboCheckbox.prop("checked", true);
            $mediaItem.addClass("checked");
          }
        });
        // Clicking the checkbox itself
        $vboCheckbox.change(function () {
          if ($(this).is(":checked")) {
            $mediaItem.addClass("checked");
          } else {
            $mediaItem.removeClass("checked");
          }
        });
      });
    },
  };
})(jQuery, Drupal);
