/**
 * @file
 * Various Media Library Scripts
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_media_library = {
    attach: function (context, settings) {
      // Add .selected class to checked media bulk operation items
      // TODO: Deprecated code?
      // $('.view-media-library .grid__content, .entities-list .item-container', context).each(function () {
      //   var $container = $(this);
      //   var $checkbox = $container.find('.form-item .form-checkbox');
      //   if ($checkbox.is(':checked')) {
      //     $container.addClass('selected');
      //   }
      //   $checkbox.on('change', function () {
      //     if ($checkbox.is(':checked')) {
      //       $container.addClass('selected');
      //     }else{
      //       $container.removeClass('selected');
      //     }
      //   });
      // });

      $(".media-library-item", context).each(function () {
        let $mediaItem = $(this);
        let $infoLink = $mediaItem.find(".media-library-item__info-link:first");
        let $infoLayer = $mediaItem.find(
          ".media-library-item__info-layer:first"
        );
        $infoLink.on("click", function (e) {
          e.preventDefault();
          e.stopPropagation();
          $mediaItem.toggleClass("media-library-item--info-layer-visible");
        });
        $infoLayer.on("click", function (e) {
          // Prevent bulk selection when clicking the info layer
          e.stopPropagation();
        });
      });
    },
  };
})(jQuery, Drupal);
