<?php

namespace Drupal\Tests\drowl_media\Functional;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\media\Entity\Media;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests drowl_media_types.
 *
 * @group drowl_media
 */
class DrowlMediaTypesFunctionalTests extends BrowserTestBase {
  use MediaTypeCreationTrait;
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Core modules first:
    'test_page_test',
    'field',
    'file',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'path',
    'taxonomy',
    'text',
    'user',
    'views',

    // Contrib modules:
    'blazy',
    'crop',
    'media_entity_file_replace',
    'media_library_edit',
    'micon',
    'fences',
    'field_group',
    'focal_point',
    'metatag',
    'pathauto',
    'responsive_image',
    'smart_trim',
    'views_linkarea',
    'drowl_media',
    'drowl_media_types',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Creates a filed on an entity type bundle.
   *
   * @param Drupal\Core\Config\Entity\ConfigEntityInterface $entityTypeBundle
   *   The entity type bundle.
   * @param string $fieldName
   *   The field name.
   * @param array $fieldTypeSettings
   *   The field type settings.
   * @param int $cardinality
   *   The maximum number of values the field can hold. Possible values are
   *   positive integers or
   *   FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED. Defaults to 1.
   */
  protected function createFieldTypeOnEntityBundle(ConfigEntityInterface $entityTypeBundle, string $fieldName, array $fieldTypeSettings, $cardinality = 1) {
    $entityType = $entityTypeBundle->getEntityType()->id();
    $bundle = $entityTypeBundle->id();

    // FIXME: $entityType results are WRONG!
    // @todo Find the right way to get "node" instead of node_type
    // and "taxonomy_term" instead of "taxonomy_vocabulary".
    // SUPER Dirty workaround to remove _type - as we don't know how to
    // get it without the suffix... but we didn't have the time:
    $entityType = str_replace('_type', '', $entityType);
    if ($entityType == 'taxonomy_vocabulary') {
      $entityType = 'taxonomy_term';
    }

    // Create field:
    FieldStorageConfig::create([
      'field_name' => $fieldName,
      'entity_type' => $entityType,
      'type' => 'entity_reference',
      'cardinality' => $cardinality,
    ])->save();

    // Create field instance:
    FieldConfig::create([
      'field_name' => $fieldName,
      'entity_type' => $entityType,
      'bundle' => $bundle,
      'settings' => $fieldTypeSettings,
    ])->save();
    return $entityTypeBundle;
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests creation of a node with a media image reference field.
   */
  public function testCreateNodeWithMediaImagereferenceField() {
    $session = $this->assertSession();
    $contentType = $this->createContentType(['type' => 'article']);
    $media_type = $this->createMediaType('image');
    $media = Media::create([
      'bundle' => $media_type->id(),
      'name' => 'my_media',
    ]);
    $media->save();
    $this->createFieldTypeOnEntityBundle($contentType, 'field_test', []);
    $node = $this->createNode([
      'type' => 'article',
    ]);
    $this->drupalGet('/node/' . $node->getOriginalId());
    $session->statusCodeEquals(200);
  }

  // @todo: Add further tests for drowl_media_types
  // Check the existence of all created media types and their fields for example.
}
