<?php

namespace Drupal\Tests\drowl_media\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests drowl_media.
 *
 * @group drowl_media
 */
class DrowlMediaFunctionalTests extends BrowserTestBase {
  use MediaTypeCreationTrait;
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'test_page_test',
    'media',
    'media_library',
    'drowl_media',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-drowl-media');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests if the drupal media library page is accessible.
   */
  public function testMediaLibraryExists() {
    $session = $this->assertSession();
    $this->drupalGet('/admin/content/media');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if the media library exists in the header of the media overview.
   */
  public function testDrowlMediaLibraryAdminLibraryExists() {
    $session = $this->assertSession();
    $this->drupalGet('/admin/content/media');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'head > link[href*=drowl_media]');
  }

}
